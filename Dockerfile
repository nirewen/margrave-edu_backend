FROM node:20.10.0-bullseye AS install

WORKDIR /app

COPY package.json .
COPY package-lock.json .

RUN npm ci

FROM install AS build

COPY src ./src

COPY tsconfig.json .
RUN npm run build

FROM node:20.10.0-bullseye AS runtime

COPY --from=build /app/dist ./dist
COPY --from=build /app/node_modules ./node_modules

EXPOSE 3000

ENV NODE_ENV=production

CMD ["node", "dist/index.js"]

USER node